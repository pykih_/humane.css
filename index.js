/* Ordered by priority, can be easily overridden with CSS files below it. */
import './css/humane-html.css';

import './css/humane-bootstrap.css';

import './css/humane-slice.css';

import './css/humane-card.css';
import './css/humane-gallery-card.css';
import './css/humane-list-card.css';

import './css/humane-cover-slice.css';
import './css/humane-slim-fit-slice.css';
import './css/humane-slide-slice.css';
import './css/humane-people-slice.css';
import './css/humane-form.css';

import './css/humane-preview.css';

import './css/humane-navigation.css';

import './css/humane-header.css';
import './css/humane-footer.css';
import './css/humane-subheader.css';

import './css/humane-nudges.css';

import './css/humane-sidemodal.css';

import './css/humane-article.css';

import './css/humane-automated.css';

import './css/humane-products.css';

import './css/humane-subscription-slice.css';

import './css/humane-10000ft.css';

import './css/humane-login.css';

import './css/humane-modal.css';

import './css/humane-motion.css';