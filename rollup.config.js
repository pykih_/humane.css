import postcss from 'rollup-plugin-postcss'

const config = [
  {
    input: 'index.js',
    output: {
      file: 'dist/humane.min.js',
      format: 'es'
    },
    plugins: [
      postcss({
        plugins: [
          require("autoprefixer"),
          require("cssnano")({
            preset: "default"
          })
        ],
        extract: true
      })
    ]
  },
];

export default config